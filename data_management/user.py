import os
import json


class User:

    def __init__(self, data: dict, api: 'SlackAPI'):
        self.api = api
        self.data = data
        self.slack_id = self.get_slack_id(data)
        self.path_to_json_database = ''

    def __repr__(self):
        return repr(self.data)

    def __iter__(self):
        return iter(self.data)

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        new_data = {key: value}
        self.update(new_data)

    def get_slack_id(self, data: dict):
        return self.data

    def update(self, new_data: dict):
        self.update_self_in_json_database()
        self.data.update(new_data)

    def update_self_in_json_database(self, new_data: dict) -> None:
        with open(self.path_to_json_database) as jsn:
            database = json.load(jsn)
        data = database[self.slack_id]
        data.update(new_data)  # updates database by reference
        with open(self.path_to_json_database, 'w') as jsn:
            json.dump(database, jsn)
