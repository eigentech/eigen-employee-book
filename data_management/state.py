from typing import Generator, List

from user import User
from sources_api import API

class State:

    def __init__(self, *args, **kwargs):
        self.api = API()
        # TODO: State shoudl be a slack_id indexed dict of User objects
        self.state = [User(user, self.api)
                      for user in self.api.query_sources(*args, **kwargs)]

    def __len__(self):
        return len(self.state)

    def __repr__(self):
        return repr(self.state)

    def __iter__(self):
        return iter(self.state)

    def __getitem__(self, filters: dict) -> Generator[dict, None, None]:
        return (s_item for s_item in self if filters.items() <= s_item.items())

    def __setitem__(self, filters: dict, new_data: dict) -> None:
        for item in self[filters]:
            item.update(new_data)

    def get_user(self, slack_id: str) -> 'User':
        return self[slack_id]

    def get_user_skills(self, slack_id: str) -> List[str]:
        user = self.get_user(slack_id)
        return user.get('skills', [])

    def set_user_skills(self, slack_id: str, skills: List[str]) -> None:
        user = self.get_user(slack_id)
        user.update({'skills': skills})

    def add_user_skills(self, slack_id: str,
                        skills_to_add: List[str]) -> None:
        user = self.get_user(slack_id)
        old_skills = self.get_user_skills(slack_id)
        new_skills = set(old_skills) + set(skills_to_add)
        user.update({'skills': list(new_skills)})

    def delete_user_skills(self, slack_id: str,
                           skills_to_delete: List[str]) -> None:
        user = self.get_user(slack_id)
        old_skills = self.get_user_skills(slack_id)
        new_skills = set(old_skills) - set(skills_to_delete)
        user.update({'skills': list(new_skills)})


def main():
    q = State()
    print(len(q.state))
    
    #q[{'status': 'TRANSFORMED', 'agr_type': 'ISDA'}] = {'status': 'INPROGRESS'}



if __name__ == '__main__':
    main()