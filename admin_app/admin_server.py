import os
from flask import Flask, json, send_file, request, make_response
from pathlib import Path


app = Flask(__name__, static_url_path='')
api_file = Path(__file__).parent.absolute()
APP_ROOT = os.path.abspath(os.path.join(api_file, '..'))

@app.route('/get_user_by_id/')
def query_queue_by_id():
    id = request.args.get('id')
    with open(APP_ROOT + '/admin_app_sample_res.json', 'r') as users_json:
        users = json.load(users_json)
    response = users[id]
    return app.response_class(
        response=json.dumps(response),
        status=200,
        mimetype='application/json'
    )

@app.route('/users/')
def list_queue():
    with open(APP_ROOT + '/admin_app_sample_res.json', 'r') as users_json:
        users = json.load(users_json)
    return app.response_class(
        response=json.dumps(users),
        status=200,
        mimetype='application/json'
    )

@app.route('/error/<int:code>')
def throw_error(code):
    status_code_lookup = {}
    for error_class in HTTPException.__subclasses__():
        status_code_lookup[error_class.code] = error_class
    return status_code_lookup.get(code, NotImplemented)()

if __name__ == "__main__":
    print(__doc__)
    print(f'{"#"*28} Admin server {"#"*28}\n')
    app.run()
