# Sweet user App
Example Eigen Employee Book application using React/Redux built for [Medium article](https://medium.com/@levifuller/how-to-build-a-scalable-user-browser-app-using-react-and-redux-in-visual-studio-code-dea8bfb3eabe)

![Eigen Employee Book App](https://image.prntscr.com/image/BXSNDUqnT6aFwuOfQvNwQw.png)

Start the app with:

```
npm start
```

