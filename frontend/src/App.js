import React, { Component } from 'react';
import EmployeeBrowser from './modules/user-browser/user-browser.container';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

class App extends Component {
  render() {
    return (
      // Provides the Material UI theme to child components
      <MuiThemeProvider>
        <EmployeeBrowser />
      </MuiThemeProvider>
    );
  }
}

export default App;
