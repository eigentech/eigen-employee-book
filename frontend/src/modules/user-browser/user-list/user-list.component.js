import React from 'react';
import {Row, Col} from 'react-bootstrap';
import userCard from '../user-card/user-card.component';
import LoaderComponent from '../../common/loader.component';

const styles = {
  userColumn: {
    marginBottom: 20
  }
}
const userListComponent = ({users, isLoading}) => {
  const userColumns = users ? users.map(user => (
    <Col style={styles.userColumn} key={user.id} xs={12} sm={4} md={3} lg={3}>
      <userCard user={user} />
    </Col>
  )) : null;
  
  return (
    <Row>
      {userColumns}
      <LoaderComponent isLoading={isLoading} />
    </Row>
  );
}

export default userListComponent;
