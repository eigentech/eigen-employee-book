
const TMDB_IMAGE_BASE_URL = (width = 300) => `https://image.tmdb.org/t/p/w${width}`;

export const updateuserPictureUrls = (userResult, width = 300) => {
  if (userResult) {
    return {
      ...userResult,
      backdrop_path: `${TMDB_IMAGE_BASE_URL(width)}${userResult.backdrop_path}`,
      poster_path: `${TMDB_IMAGE_BASE_URL(width)}${userResult.poster_path}`,
    }
  }
  return {};
};

export const getusersList = (usersResponse) => {
  return !!usersResponse ? ([
    ...usersResponse.results.map(userResult => updateuserPictureUrls(userResult))
  ]) : null;
}
