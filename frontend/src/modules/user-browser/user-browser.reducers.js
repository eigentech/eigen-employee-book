import {combineReducers} from 'redux';
import { createReducer, createAsyncReducer } from '../common/redux.helpers';
import { keys as userActionKeys } from './user-browser.actions';
import userModalReducer from './user-modal/user-modal.reducer';

// This will create a new state with both the existing 
// users and new pages of users
const usersSuccessReducer = (state, action) => {
  console.log(state.response)
  const existingusers = state.response ? state.response.results : [];
  // Create a new state object to be returned
  // When creating the new state, be sure to include any
  // existing properties we want to persist
  return {
    ...state,
    isLoading: false,
    response: {
      ...action.response,
      results: [
        ...existingusers,
        ...action.response.results
      ]
    }
  };
}

// Combines our Eigen Employee Book related reducers to build our EmployeeBrowser reducer
const EmployeeBrowserReducer = combineReducers({
  userModal: userModalReducer,
  topusers: createAsyncReducer(userActionKeys.GET_TOP_userS, {
    [`${userActionKeys.LIST_USERS}_SUCCESS`]: usersSuccessReducer
  }),
  userSearch: createAsyncReducer(userActionKeys.SEARCH_userS, {
    [`${userActionKeys.SEARCH_USERS}_SUCCESS`]: usersSuccessReducer
  }),
  userDetails: createAsyncReducer(userActionKeys.GET_user_DETAILS),
});

export default EmployeeBrowserReducer;
