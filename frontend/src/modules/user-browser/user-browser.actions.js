import {createAsyncActionCreator} from '../common/redux.helpers';
import * as userService from './user-browser.service';

export const keys = {
  'SEARCH_USERS': 'SEARCH_USERS',
  'LIST_USERS': 'LIST_USERS'
};

export const searchUsersAndSkill = (query, page) => createAsyncActionCreator(
  keys.SEARCH_USERS,
  userService.searchUsersAndSkill, 
  {query, page}
);


export const listUsers = (query, page) => createAsyncActionCreator(
  keys.LIST_USERS,
  userService.listUsers, 
  {query, page}
);

