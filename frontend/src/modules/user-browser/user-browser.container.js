import React from 'react';
import {connect} from 'react-redux';
import {Grid, Row, Col} from 'react-bootstrap';
import {AppBar, TextField, RaisedButton} from 'material-ui';
import * as userActions from './user-browser.actions';
import * as userHelpers from './user-browser.helpers';
import userList from './user-list/user-list.component';
import * as scrollHelpers from '../common/scroll.helpers';
import userModal from './user-modal/user-modal.container';

class EmployeeBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      currentusers: []
    };
    // Binds the handleScroll to this class (EmployeeBrowser)
    // which provides access to EmployeeBrowser's props
    // Note: You don't have to do this if you call a method
    // directly from a lifecycle method
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.onscroll = this.handleScroll;
    this.props.listUsers(this.state.currentPage)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    const {topusers} = this.props;
    if (!topusers.isLoading) {
      let percentageScrolled = scrollHelpers.getPercentageScrolledDown(window);
      if (percentageScrolled > .8) {
        const nextPage = this.state.currentPage + 1;
        this.setState({currentPage: nextPage});
      }
    }
  }

  render() {
    const {topusers} = this.props;
    const users = userHelpers.getusersList(topusers.response);

    return (
      <div>
        <AppBar title='Eigen Employee Book' />
        <Grid>
          <Row>
            <p>Search will go here</p>
          </Row>
          <Row>
            <userList users={users} isLoading={topusers.isLoading} />
          </Row>
        </Grid>
        <userModal />
      </div>
    );
  }
}

export default connect(
  // Map nodes in our state to a properties of our component
  (state) => ({
    topusers: state.EmployeeBrowser.topusers
  }),
  // Map action creators to properties of our component
  { ...userActions }
)(EmployeeBrowser);
