import { keys } from './user-modal.actions';
import { createReducer } from '../../common/redux.helpers';

// Placeholder reducer for our user modal
const userModalReducer = createReducer({ isOpen: false, userId: undefined }, {
  [keys.OPEN_user_MODAL]: (state, action) => ({
    isOpen: true,
    userId: action.userId
  }),
  [keys.CLOSE_user_MODAL]: (state, action) => ({
    // Persist the userId (and any other state tree objects)
    ...state,
    isOpen: false
  })
});

export default userModalReducer;
