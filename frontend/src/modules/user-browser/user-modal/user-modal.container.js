import React from 'react';
import {connect} from 'react-redux';
import { Dialog } from 'material-ui';
import _ from 'lodash';
import { closeuserModal } from './user-modal.actions';
import * as userHelpers from '../user-browser.helpers';
import Loader from '../../common/loader.component';

const styles = {
  // Can use functions to dynamically build our CSS
  dialogContent: (backgroundUrl) => ({
    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(${backgroundUrl})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100%',
    height: '100%',
    minHeight: 400,
    color: 'white',
    padding: 10
  })
}

class userModalContainer extends React.Component {
  // Triggered right after a property is changed


  render() {
    const {isOpen, closeuserModal, isLoading} = this.props;
    const loadingStatus = isLoading ? 'loading' : 'hide';
    const user = userHelpers.updateuserPictureUrls(this.props.user);
    const genres = (user && user.genres) ? user.genres.map(genre => genre.name).join(', ') : '';

    return (
      <Dialog
        autoScrollBodyContent={true}
        title={null}
        modal={false}
        open={isOpen}
        onRequestClose={closeuserModal}
      >
        <Loader isLoading={isLoading}>
          <div style={styles.dialogContent(user.backdrop_path)}>
            <h1>{user.title}</h1>
            <h5>{genres}</h5>
            <p>{user.overview}</p>
            <p>Popularity: {user.popularity}</p>
            <p>Budget: ${user.budget}</p>
          </div>
        </Loader>
    </Dialog>
    );
  }
}
// "connect" our user modal to the component store
export default connect(
  // Map nodes in our state to a properties of our component
  (state) => ({
    // Using lodash get, recursively check that a property is defined
    // before try to access it - if it is undefined, it will return your default value
    // _.get(object, 'path.to.targets[0].neat.stuff', defaultValue)
    isOpen: _.get(state, 'EmployeeBrowser.userModal.isOpen', false),
    userId: _.get(state, 'EmployeeBrowser.userModal.userId'),
    user: _.get(state, 'EmployeeBrowser.userDetails.response', {}),
    isLoading: _.get(state, 'EmployeeBrowser.userDetails.isLoading', false),
  }),
  // Map an action to a prop, ready to be dispatched
  { closeuserModal }
)(userModalContainer);
