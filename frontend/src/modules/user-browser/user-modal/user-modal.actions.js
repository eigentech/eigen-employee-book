// List of user modal action type keys
export const keys = {
  'OPEN_user_MODAL': 'OPEN_user_MODAL',
  'CLOSE_user_MODAL': 'CLOSE_user_MODAL',
}

// Opens the <userModal /> with a userId
export const openuserModal = (userId) => {
  return {
    type: keys.OPEN_user_MODAL,
    userId
  };
}

// Closes the <userModal />
export const closeuserModal = () => {
  return {
    type: keys.CLOSE_user_MODAL
  };
}
