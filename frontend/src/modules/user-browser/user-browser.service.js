// WARNING: Don't check your actual API key into GitHub
const EIGEN_DB_BASE_UL = 'http://127.0.0.1:5000/';

const createEigenDbUrl = (relativeUrl, queryParams) => {
  let baseUrl = `${EIGEN_DB_BASE_UL}${relativeUrl}`;
  if (queryParams) {
    Object.keys(queryParams)
      .forEach(paramName => baseUrl += `&${paramName}=${queryParams[paramName]}`);
  }
  return baseUrl;
}


export const searchUsersAndSkill = async ({ page, query}) => {
  const fullUrl = createEigenDbUrl('/user', {
    page,
    query
  });
  return fetch('http://127.0.0.1:5000/get_user_by_id/?id=1');
}


export const listUsers = async ({ page, query}) => {
  const fullUrl = createEigenDbUrl('/user', {
    page,
    query
  });
  return fetch('http://127.0.0.1:5000/users/');
}